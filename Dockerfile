FROM etherpad/etherpad:1.8.16

USER root

# install abiword
RUN export DEBIAN_FRONTEND=noninteractive; \
    mkdir -p /usr/share/man/man1 && \
    apt-get -qq update && \
    apt-get -qq --no-install-recommends install abiword && \
    apt-get -qq clean && \
    rm -rf /var/lib/apt/lists/*

# add entrypoint
COPY entrypoint.sh /

USER etherpad

ENTRYPOINT ["/entrypoint.sh"]